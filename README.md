# `Greenberry GraphQL server`

## Definition
This project is used for creating a `GraphQL` based `API`, or `GraphQL Server`, to be used with the project `Greenberry Assignment`.

#
## Prerequisites
In order to run this project, a few prerequisites are required:
 1. Install `Node` for running the server.
 3. Install `Yarn` / `NPM` for the dependencies.
 4. Install `Typescript` to compile the code

#
## Using the common library
For generic code and interfaces used by schema's / ORM, you will need to link the `greenberry-common` package.

To do so, follow these steps:

1. Clone the `greenberry-common` repository somewhere on your local drive.
2. Change the directory to the common repository: `cd /path/to/common-repo`.
3. Link the directory: `yarn link`
4. Change the directory to this repository again: `cd /path/to/this-repo`.
5. Finish the linking process: `yarn link "greenberry-common"`.

If everything went well there will be a new directory in `node_modules` called `greenberry-common`, which is a symlink to the local directory and thus will update as soon as a file changes in there. This will also cause `IntelliSense` to be fired right away when the interface for a model changes.

#
## Running the server
To run the server, simply run `yarn start`, which will fire the following command:

```
tsc && node ./dist
```

`tsc` will cause the typescript code base to be compiled to javascript, which will be located in the `/dist` folder due to the settings made in `/tsconfig.json`.

`node` will start node.js in the context of the `/dist` folder.

#
### Changes
When changes are made in the `GraphQL server`, the thread needs to be terminated (`CTRL + C`) and run again with the command above.

#
## Building the server
To build the server for production, run `yarn build`, which will fire `tsc` to compile the typescript sources into the `/dist` folder of the project. This is handled by the options set in `/tsconfig.json`.

#
## Running tests
To run the tests, simply run `yarn test`. If you would like to generate LCOV code coverage reports using istanbul as well, run `yarn coverage`.