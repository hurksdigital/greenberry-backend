import ORM from './src/core/ORM'
import FilmSchema from './src/schemas/FilmSchema'
import PersonSchema from './src/schemas/PersonSchema'
import PlanetSchema from './src/schemas/PlanetSchema'
import SpeciesSchema from './src/schemas/SpeciesSchema'
import StarshipSchema from './src/schemas/StarshipSchema'
import VehicleSchema from './src/schemas/VehicleSchema'
import Tasks from './src/core/tasks/Tasks'
import Server from './src/core/Server'
import SwapiUpdateDatabaseTask from './src/integrations/swapi/SwapiUpdateDatabaseTask'
import PersonResolver from './src/resolvers/person/PersonResolver'
import SpeciesResolver from './src/resolvers/SpeciesResolver'
import FilmResolver from './src/resolvers/FilmResolver'
(global as any)['fetch'] = require('node-fetch')

/**
 * The time at which the server started
 */
const startTimeMS = new Date().getTime()

/**
 * Bootstrap the application
 */
Promise.all([
    ORM.initialize([
        FilmSchema,
        PersonSchema,
        PlanetSchema,
        SpeciesSchema,
        StarshipSchema,
        VehicleSchema
    ])
]).then(() => {
    Tasks.initialize([
        new SwapiUpdateDatabaseTask()
    ]).then(() => {
        Server.initialize([
            PersonResolver,
            SpeciesResolver,
            FilmResolver
        ]).then(() => {
            console.log(`Time to start server: ${new Date().getTime() - startTimeMS}ms.`)
            console.log('Server running on: http://localhost:4000.')
        }).catch((ex) => {
            console.error('Could not start GraphQL server:')
            console.error(ex)
        })
    }).catch((ex) => {
        console.error('Could not start tasks:', ex)
    })
}).catch((ex) => {
    console.error('Could not start core functionality for GraphQL server:')
    console.error(ex)
})