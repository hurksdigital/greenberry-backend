import ORM from './ORM'
import FilmSchema from '../schemas/FilmSchema'
import { fail } from 'assert'
import PersonSchema from '../schemas/PersonSchema'
import PlanetSchema from '../schemas/PlanetSchema'
import SpeciesSchema from '../schemas/SpeciesSchema'
import StarshipSchema from '../schemas/StarshipSchema'
import VehicleSchema from '../schemas/VehicleSchema'

describe('ORM functionality', () => {
    it('should initialize ORM without problems for all schemas', (done) => {
        ORM.initialize([
            FilmSchema,
            PersonSchema,
            PlanetSchema,
            SpeciesSchema,
            StarshipSchema,
            VehicleSchema
        ]).then(() => {
            done()
        }).catch((ex) => {
            console.error(ex)
            fail()
        })
    })
})