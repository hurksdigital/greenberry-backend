import { createConnection, EntitySchema } from 'typeorm'

/**
 * The ORM configuration for the project.
 * 
 * @author Stan Hurks
 * @see https://typeorm.io/#/
 */
export default abstract class ORM {

    /**
     * Initialize the ORM
     */
    public static initialize(entities: Array<string | Function | EntitySchema<any>>): Promise<void> {
        console.info('Initializing ORM...')

        return new Promise((resolve, reject) => {
            createConnection({
                type: 'sqlite',
                entities,
                database: './database.sqlite',
                synchronize: true,
                logging: false
            }).then(() => {
                resolve()
            }).catch((ex) => {
                console.error('Could not initialize ORM:')

                reject(ex)
            })
        })
    }
}