# `Core functionality`

## Definition
This folder contains all the files that represent the core functionality for the application. E.g.:
 - ORM / Database configuration
 - TypeGraphQL server configuration
 - Configuration classes for core server functionality
 - Classes that build up the core functionality (E.g.: Http, Storage, etc.)