import Server from './Server'
import { fail } from 'assert'
import PersonResolver from '../resolvers/person/PersonResolver'
import SpeciesResolver from '../resolvers/SpeciesResolver'
import FilmResolver from '../resolvers/FilmResolver'

describe('Server functionality', () => {
    it('should start the server without problems and create the GraphQL schema.', (done) => {
        Server.initialize([
            PersonResolver,
            SpeciesResolver,
            FilmResolver
        ]).then(() => {
            done()
        }).catch(() => {
            fail()
        })
    })
})