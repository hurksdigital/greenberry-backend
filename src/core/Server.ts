import { GraphQLServer } from 'graphql-yoga'
import "reflect-metadata"
import { buildSchema } from 'type-graphql'

/**
 * The core server functionality for the GraphQL server.
 * 
 * @author Stan Hurks
 */
export default abstract class Server {

    /**
     * Initialize the server
     */
    public static async initialize(resolvers: (string | Function)[]): Promise<void> {
        return new Promise((resolve, reject) => {
            console.log('Initializing GraphQL Server...')

            buildSchema({
                resolvers,
                emitSchemaFile: true,
                authMode: 'null'
            }).then((schema) => {
                const server = new GraphQLServer({
                    schema
                })

                server.start(() => {
                    resolve()
                }).catch((e) => {
                    console.error('Could not initialize express.js server:')
                    reject(e)
                })
            }).catch((ex) => {
                console.error('Could not initialize GraphQL schema:')
                reject(ex)
            })
        })
    }
}