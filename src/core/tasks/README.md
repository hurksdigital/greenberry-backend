# `Tasks`

## Definition
A task is a process that runs in the background, similar to a `cron job`.

In every application there can be several `Tasks` that run functionality every `x milliseconds`.

For example, you could have a `Task` that:
 - Runs every 24 hours to:
    - Retrieve all users that have signed up for the newsletter
    - Check whether the user has already received the newsletter
    - Send an e-mail to users that did not receive the newsletter yet.

#
## Implementation
You can use the following steps to implement a `Task` in the server:

1. Add a `tasks` directory in the relevant directory for the task, to keep everything together.
2. Create a typescript class for the task that extends the `src/core/tasks/Task` interface.
3. Add an instance of the new `Task class` in the array of `Tasks`, found in `src/index.ts`.

For example, if you would like to add a `Task` to retrieve all entries of `Contentful` and add/update them in the database every 15 minutes, you should follow the following steps:

1. Add a `tasks` directory in `src/integrations/contentful` (given that is the root directory for the contentful integration.)
2. Create the typescript class `src/integrations/contentful/tasks/ContentfulUpdateDatabaseTask.ts`, which would look something like this:
```
import Task from '../../../core/tasks/Task'

export default abstract class ContentfulUpdateDatabaseTask implements Task {

    // The interval to run the task in MS
    public readonly interval = 15 * 60 * 1000

    // Optional: Initialize the task
    public initialize(): Promise<void> {
        return new Promise((resolve, reject) => {
            // Logic to initialize the task
        })
    }

    // Run the task
    public run(): Promise<void> {
        return new Promise((resolve, reject) => {
            // Some logic
            resolve()
        })
    }
}
```

This would be the same as:
```
import Task from '../../../core/tasks/Task'

export default abstract class ContentfulUpdateDatabaseTask implements Task {

    // The interval to run the task in MS
    public readonly interval = 15 * 60 * 1000

    // Optional: Initialize the task
    public async initialize() {
        // Logic to initialize the task
    }

    // Run the task
    public async run() {
        // Some logic
    }
}
```

Although, when working with `Promises` it has a clearer syntax for `rejecting` the promise.

3. Add an instance of `ContentfulUpdateDatabaseTask` in the entry point of the application (`index.ts`):
```
...
    Promise.all([
        ...
        Tasks.initialize([
            ...
            new ContentfulUpdateDatabaseTask() 
            ...
        ])
    ]).then(() => {
        ...
    }).catch((ex) => {
        ...
    })
...
```