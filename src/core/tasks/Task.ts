/**
 * The interface for a task to be executed as a background service.
 * 
 * @author Stan Hurks
 */
export default interface Task {

    /**
     * The interval in MS in which a task should be runned.
     */
    readonly interval: number

    /**
     * Optional: Initialize the task
     */
    initialize?: () => Promise<void> | undefined

    /**
     * Run the functionality of the task
     */
    run: () => Promise<void>
}