import Task from './Task'

/**
 * All functionality regarding `Tasks`.
 * 
 * @author Stan Hurks
 * @see ./README.md
 */
export default abstract class Tasks {

    /**
     * Initialize all tasks
     */
    public static initialize(tasks: Task[]): Promise<void> {
        return new Promise((resolve, reject) => {
            console.log('Initializing tasks...')

            // The promises of all `tasks` to be done before resolving the main `Promise`.
            const promises: Promise<void>[] = []

            // Initialize all the tasks with implementations to initialize.
            for (const task of tasks.filter((task) => task.initialize)) {
                promises.push((task as any).initialize())
            }

            // Wait for all tasks to be initialized.
            Promise.all(promises).then(() => {

                // Run all the tasks
                for (const task of tasks) {

                    // Make sure the task runs for every given interval
                    setInterval(() => {
                        task.run()
                    }, task.interval)

                    // Run the task upon initialization
                    task.run()
                }
                resolve()
            }).catch((ex) => {
                console.error('Could not initialize tasks.')
                reject(ex)
            })
        })
    }
}