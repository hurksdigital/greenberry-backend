# `Integrations`

## Definition
In this directory all logics for integrations are implmented.

For example: If you would like to create an integration with the `Content Delivery API` of `Contentful` for authorization, you would create a directory in here: `src/integrations/contentful/content-delivery/`, containing all of the functionality for the integration.

When you categorise the integrations like this, the code base will stay as clean as possible. Which is helpful for new developers and also for when you jump back into the code that you made a while ago.