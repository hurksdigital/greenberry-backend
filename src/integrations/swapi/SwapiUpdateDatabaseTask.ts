import Task from '../../core/tasks/Task'
import { getConnection, getRepository } from 'typeorm'
import Planet from 'greenberry-common/models/entity/Planet'
import Film from 'greenberry-common/models/entity/Film'
import Person from 'greenberry-common/models/entity/Person'
import Species from 'greenberry-common/models/entity/Species'
import Starship from 'greenberry-common/models/entity/Starship'
import Vehicle from 'greenberry-common/models/entity/Vehicle'
import PersonSchema from '../../schemas/PersonSchema'
import FilmSchema from '../../schemas/FilmSchema'
import PlanetSchema from '../../schemas/PlanetSchema'
import SpeciesSchema from '../../schemas/SpeciesSchema'
import StarshipSchema from '../../schemas/StarshipSchema'
import VehicleSchema from '../../schemas/VehicleSchema'

/**
 * Represents all the entries from the SWAPI.
 */
type AllSwapiEntries = {
    planets: Planet[]
    films: Film[]
    people: Person[]
    species: Species[]
    starships: Starship[]
    vehicles: Vehicle[]
}

/**
 * The task responsible for pulling data from SWAPI.
 * 
 * This will put all data found in a Sqlite database, reason being that the data will always be consistent.
 * 
 * It does so by running a background service which pulls the data daily from SWAPI,
 * clears the database and seeds it with the data provided.
 * 
 * @author Stan Hurks
 */
export default class SwapiUpdateDatabaseTask implements Task {

    /**
     * Fire once per day
     */
    public readonly interval = 24 * 60 * 60 * 1000

    /**
     * The regex to strip an ID from the `url` obtained in the call.
     */
    private readonly idRegex: RegExp = /([a-z]|[A-Z]|\/|\.|:)/g

    /**
     * Run the task
     */
    public run(): Promise<void> {
        return new Promise((resolve, reject) => {
            console.log('Updating database from SWAPI...')

            this.clearDatabase().then(() => {
                this.getDataFromSwapi().then((data) => {
                    this.saveDataFromSwapi(data).then(() => {
                        resolve()
                    })
                })
            }).catch(() => {
                reject()
            })
        })
    }

    /**
     * Clears the entire database.
     */
    private clearDatabase(): Promise<void> {
        return new Promise((resolve, reject) => {
            console.log('Clearing database...')

            getConnection()
                .synchronize(true)
                .then(() => {
                    console.log('Cleared database.')

                    resolve()
                })
                .catch((ex) => {
                    console.error('Could not clear database: ', ex)

                    reject()
                })
        })
    }

    /**
     * Get all data from the SWAPI.
     */
    private getDataFromSwapi(): Promise<AllSwapiEntries> {
        return new Promise((resolve, reject) => {
            const promises = [
                this.getRawDataForModel('people'),
                this.getRawDataForModel('planets'),
                this.getRawDataForModel('films'),
                this.getRawDataForModel('species'),
                this.getRawDataForModel('vehicles'),
                this.getRawDataForModel('starships')
            ]
            Promise.all(promises)
                .then((responses) => {
                    // The response for the promise
                    let response: AllSwapiEntries = {
                        planets: [],
                        films: [],
                        people: [],
                        species: [],
                        starships: [],
                        vehicles: []
                    }

                    let [people, planets, films, species, vehicles, starships] = responses

                    // Map all entities
                    planets = planets.map((planets: any) => this.mapPlanet(planets, responses, response))
                    films = films.map((film: any) => this.mapFilm(film, responses, response))
                    species = species.map((s: any) => this.mapSpecies(s, responses, response))
                    starships = starships.map((starship: any) => this.mapStarship(starship, responses, response))
                    vehicles = vehicles.map((vehicle: any) => this.mapVehicle(vehicle, responses, response))
                    people = people.map((person: any) => this.mapPerson(person, responses, response))

                    resolve(response)
                })
                .catch((ex) => {
                    console.error('Could not get responses: ', ex)

                    reject()
                })
        })
    }

    /**
     * Saves all data from SWAPI in the database
     * @param data the data
     */
    private saveDataFromSwapi(data: AllSwapiEntries): Promise<void> {
        return new Promise((resolve, reject) => {
            console.log('Saving data from SWAPI...')

            try {
                const promises = [
                    getRepository(FilmSchema).save(data.films),
                    getRepository(PlanetSchema).save(data.planets),
                    getRepository(SpeciesSchema).save(data.species),
                    getRepository(StarshipSchema).save(data.starships),
                    getRepository(VehicleSchema).save(data.vehicles),
                    getRepository(PersonSchema).save(data.people)
                ] as Promise<any>[]
                Promise.all(promises)
                    .then(() => {
                        console.log('Done !')

                        resolve()
                    })
                    .catch((ex) => {
                        console.error('Could not save data from SWAPI: ', ex)

                        reject()
                    })
            } catch (err) {
                console.error(err)
            }
        })
    }

    /**
     * Get the raw data for a given model.
     * 
     * This will go through every page in the SWAPI for the given entity, until there are no more pages left.
     * 
     * @param modelName the name for the model to get the raw data for
     * @param page the page number
     * @param results the results
     */
    private getRawDataForModel(modelName: 'people' | 'planets' | 'films' | 'species' | 'vehicles' | 'starships', page?: number, results?: any[]): Promise<any> {
        return new Promise((resolve, reject) => {
            console.log(`Fetching raw data for model: ${modelName}, page: ${page || 1}`)

            fetch(`https://swapi.co/api/${modelName}/?page=${page || 1}`).then((response) => response.json()).then((response) => {
                let newResults = (results || []).concat(response.results)

                if (response.next === null) {
                    resolve(newResults)
                } else {
                    resolve(this.getRawDataForModel(modelName, (page || 1) + 1, newResults))
                }
            }).catch((ex) => {
                console.log('Too many requests, waiting 1s...')

                setTimeout(() => {
                    resolve(this.getRawDataForModel(modelName, page, results))
                }, 1000)
            })
        })
    }

    /**
     * Map a film from a raw entry from SWAPI.
     * 
     * @param film the film
     * @param responses all the responses from SWAPI
     * @param response the response, containing all cached entries
     */
    private mapFilm(film: any, responses: any[], response: AllSwapiEntries): FilmSchema {
        let [people, planets, films, species, vehicles, starships] = responses

        // If the URL is used, retrieve the entry from the response
        if (typeof film === 'string') {
            return this.mapFilm(films.filter((f: any) => f.url === film)[0], responses, response)
        }

        const id = film.url.replace(this.idRegex, '')

        // Return from responses if available
        if (response.films.filter((film) => film.uuid === id).length > 0) {
            return response.films.filter((film) => film.uuid == id)[0]
        }

        film = Object.assign(
            new FilmSchema(),
            {
                ...film,
                uuid: id,
                release_date: new Date(film.release_date),
                created: new Date(film.created),
                edited: new Date(film.edited),
            } as FilmSchema
        )
        response.films.push(film)
        return film
    }

    /**
     * Map a person from a raw entry from SWAPI.
     * 
     * @param person the person
     * @param responses all the responses from SWAPI
     * @param response the response, containing all cached entries
     */
    private mapPerson(person: any, responses: any[], response: AllSwapiEntries): PersonSchema {
        let [people, planets, films, species, vehicles, starships] = responses

        // If the URL is used, retrieve the entry from the response
        if (typeof person === 'string') {
            return this.mapPerson(people.filter((p: any) => p.url === person)[0], responses, response)
        }

        const id = person.url.replace(this.idRegex, '')

        // Return from responses if available
        if (response.people.filter((p) => p.uuid === id).length > 0) {
            return response.people.filter((p) => p.uuid == id)[0]
        }

        person = Object.assign(
            new PersonSchema(),
            {
                ...person,
                uuid: id,
                films: person.films.map((film: any) => this.mapFilm(film, responses, response)),
                species: person.species.map((s: any) => this.mapSpecies(s, responses, response)),
                vehicles: person.vehicles.map((vehicle: any) => this.mapVehicle(vehicle, responses, response)),
                starships: person.starships.map((s: any) => this.mapStarship(s, responses, response)),
                homeworld: this.mapPlanet(person.homeworld, responses, response),
                created: new Date(person.created),
                edited: new Date(person.edited),
            } as PersonSchema
        )
        response.people.push(person)
        return person
    }

    /**
     * Map a planet from a raw entry from SWAPI.
     * 
     * @param planet the planet
     * @param responses all the responses from SWAPI
     * @param response the response, containing all cached entries
     */
    private mapPlanet(planet: any, responses: any[], response: AllSwapiEntries): PlanetSchema | null {
        let [people, planets, films, species, vehicles, starships] = responses

        // If the URL is used, retrieve the entry from the response
        if (typeof planet === 'string') {
            return this.mapPlanet(planets.filter((p: any) => p.url === planet)[0], responses, response)
        }

        if (planet === null) {
            return null
        }

        const id = planet.url.replace(this.idRegex, '')

        // Return from responses if available
        if (response.planets.filter((planet) => planet.uuid === id).length > 0) {
            return response.planets.filter((planet) => planet.uuid == id)[0]
        }

        planet = Object.assign(
            new PlanetSchema(),
            {
                ...planet,
                uuid: id,
                films: planet.films.map((film: any) => this.mapFilm(film, responses, response)),
                created: new Date(planet.created),
                edited: new Date(planet.edited),
            } as PlanetSchema
        )
        response.planets.push(planet)
        return planet
    }

    /**
     * Map a species from a raw entry from SWAPI.
     * 
     * @param species the species
     * @param responses all the responses from SWAPI
     * @param response the response, containing all cached entries
     */
    private mapSpecies(species: any, responses: any[], response: AllSwapiEntries): SpeciesSchema {
        let [people, planets, films, speciesResponse, vehicles, starships] = responses

        // If the URL is used, retrieve the entry from the response
        if (typeof species === 'string') {
            return this.mapSpecies(speciesResponse.filter((s: any) => s.url === species)[0], responses, response)
        }

        const id = species.url.replace(this.idRegex, '')

        // Return from responses if available
        if (response.species.filter((s) => s.uuid === id).length > 0) {
            return response.species.filter((s) => s.uuid == id)[0]
        }

        species = Object.assign(
            new SpeciesSchema(),
            {
                ...species,
                uuid: id,
                created: new Date(species.created),
                edited: new Date(species.edited),
            } as SpeciesSchema
        )
        response.species.push(species)
        return species
    }

    /**
     * Map a starship from a raw entry from SWAPI.
     * 
     * @param starship the starship
     * @param responses all the responses from SWAPI
     * @param response the response, containing all cached entries
     */
    private mapStarship(starship: any, responses: any[], response: AllSwapiEntries): StarshipSchema {
        let [people, planets, films, species, vehicles, starships] = responses

        // If the URL is used, retrieve the entry from the response
        if (typeof starship === 'string') {
            return this.mapStarship(starships.filter((s: any) => s.url === starship)[0], responses, response)
        }

        const id = starship.url.replace(this.idRegex, '')

        // Return from responses if available
        if (response.starships.filter((starship) => starship.uuid === id).length > 0) {
            return response.starships.filter((starship) => starship.uuid == id)[0]
        }

        starship = Object.assign(
            new StarshipSchema(),
            {
                ...starship,
                uuid: id,
                films: starship.films.map((film: any) => this.mapFilm(film, responses, response)),
                created: new Date(starship.created),
                edited: new Date(starship.edited),
            } as StarshipSchema
        )
        response.starships.push(starship)
        return starship
    }

    /**
     * Map a vehicle from a raw entry from SWAPI.
     * 
     * @param vehicle the vehicle
     * @param responses all the responses from SWAPI
     * @param response the response, containing all cached entries
     */
    private mapVehicle(vehicle: any, responses: any[], response: AllSwapiEntries): VehicleSchema {
        let [people, planets, films, species, vehicles, starships] = responses

        // If the URL is used, retrieve the entry from the response
        if (typeof vehicle === 'string') {
            return this.mapVehicle(vehicles.filter((v: any) => v.url === vehicle)[0], responses, response)
        }

        const id = vehicle.url.replace(this.idRegex, '')

        // Return from responses if available
        if (response.vehicles.filter((vehicle) => vehicle.uuid === id).length > 0) {
            return response.vehicles.filter((vehicle) => vehicle.uuid == id)[0]
        }

        vehicle = Object.assign(
            new VehicleSchema(),
            {
                ...vehicle,
                uuid: id,
                films: vehicle.films.map((film: any) => this.mapFilm(film, responses, response)),
                created: new Date(vehicle.created),
                edited: new Date(vehicle.edited),
            } as VehicleSchema
        )
        response.vehicles.push(vehicle)
        return vehicle
    }
}