import { Resolver, Query } from 'type-graphql'
import FilmSchema from '../schemas/FilmSchema'
import { getManager } from 'typeorm'

/**
 * The resolver for the film schema.
 * 
 * @author Stan Hurks
 */
@Resolver(of => FilmSchema)
export default class FilmResolver {

    /**
     * Get the list of all films for the select in the front-end
     */
    @Query(returns => [FilmSchema])
    public async getFilms(): Promise<FilmSchema[]> {
        return await getManager().find(FilmSchema).then((res) => {
            return res
        }).catch((err) => {
            console.error(err)
            return []
        })
    }
}