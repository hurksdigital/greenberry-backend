import { Resolver, Query } from 'type-graphql'
import SpeciesSchema from '../schemas/SpeciesSchema'
import { getManager } from 'typeorm'

/**
 * The resolver for the species schema.
 * 
 * @author Stan Hurks
 */
@Resolver(of => SpeciesSchema)
export default class SpeciesResolver {

    /**
     * Get the list of all species for the select in the front-end
     */
    @Query(returns => [SpeciesSchema])
    public async getSpecies(): Promise<SpeciesSchema[]> {
        return await getManager().find(SpeciesSchema).then((res) => {
            return res
        }).catch((ex) => {
            console.error('Could not get species: ', ex)
            return []
        })
    }
}