import { Resolver, Query, Arg } from 'type-graphql'
import PersonSchema from '../../schemas/PersonSchema'
import { getManager } from 'typeorm'
import GetPeopleInput from './input/GetPeopleInput'

/**
 * The resolver for the user schema.
 * 
 * @author Stan Hurks
 */
@Resolver(of => PersonSchema)
export default class PersonResolver {

    /**
     * Get the list of all characters for the select in the front-end
     */
    @Query(returns => [PersonSchema])
    public async getCharacters(): Promise<PersonSchema[]> {
        return await getManager().find(PersonSchema).then((res) => {
            return res
        }).catch((ex) => {
            console.error('Could not get characters: ', ex)
            return []
        })
    }

    /**
     * Get the list of people from the database filtered by the arguments.
     */
    @Query(returns => [PersonSchema])
    public async getPeople(@Arg('input') input: GetPeopleInput): Promise<PersonSchema[]> {
        return await getManager().find(PersonSchema, {
            where: {
                ...(input.characterUuid ? {
                    uuid: input.characterUuid
                } : undefined)
            },
            relations: [
                'films',
                'species',
                'starships',
                'vehicles'
            ]
        }).then((res) => {
            if (input.filmUuid) {
                res = res.filter((p) => p.films.filter(f => f.uuid === input.filmUuid).length)
            }
            if (input.speciesUuid) {
                res = res.filter((p) => p.species.filter(s => s.uuid === input.speciesUuid).length)
            }
            return res
        }).catch((ex) => {
            console.error('Could not get people.', ex)
            return []
        })
    }
}