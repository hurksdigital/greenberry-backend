import { InputType, Field } from 'type-graphql'

/**
 * The input type for the GetPeople query.
 * 
 * @author Stan Hurks
 */
@InputType()
export default class GetPeopleInput {

    /**
     * The uuid of the film to filter on
     */
    @Field({ nullable: true })
    filmUuid?: string

    /**
     * The uuid of the species to filter on
     */
    @Field({ nullable: true })
    speciesUuid?: string

    /**
     * The uuid of the character to filter on
     */
    @Field({ nullable: true })
    characterUuid?: string
}