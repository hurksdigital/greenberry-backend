import Film from 'greenberry-common/models/entity/Film'
import {
    ObjectType, Field, Int
} from 'type-graphql'
import {
    Entity, PrimaryGeneratedColumn, Column
} from 'typeorm'

/**
 * The schema and ORM implementation for the `Film` entity.
 * 
 * @author Stan Hurks
 */
@ObjectType()
@Entity({
    name: 'film'
})
export default class FilmSchema implements Film {

    /**
     * The UUID of the entity.
     */
    @PrimaryGeneratedColumn('uuid')
    @Field()
    uuid: string

    /**
     * The title of this film
     */
    @Column()
    @Field()
    title: string

    /**
     * The episode number of this film.
     */
    @Column()
    @Field(type => Int)
    episode_id: number

    /**
     * The opening paragraphs at the beginning of this film.
     */
    @Column()
    @Field()
    opening_crawl: string

    /**
     * The name of the director of this film.
     */
    @Column()
    @Field()
    director: string

    /**
     * The name(s) of the producer(s) of this film.Comma separated.
     */
    @Column()
    @Field()
    producer: string

    /**
     * The date format of film release at original creator country.
     */
    @Column({
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP'
    })
    @Field()
    release_date: Date

    /**
     * The time that this resource was created.
     */
    @Column({
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP'
    })
    @Field()
    created: Date

    /**
     * The time that this resource was edited.
     */
    @Column({
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP'
    })
    @Field()
    edited: Date
}