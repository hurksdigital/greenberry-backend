import Person from 'greenberry-common/models/entity/Person'
import Film from 'greenberry-common/models/entity/Film'
import Species from 'greenberry-common/models/entity/Species'
import Starship from 'greenberry-common/models/entity/Starship'
import Vehicle from 'greenberry-common/models/entity/Vehicle'
import { ObjectType, Field } from 'type-graphql'
import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable, ManyToOne } from 'typeorm'
import FilmSchema from './FilmSchema'
import SpeciesSchema from './SpeciesSchema'
import StarshipSchema from './StarshipSchema'
import VehicleSchema from './VehicleSchema'
import Planet from 'greenberry-common/models/entity/Planet'
import PlanetSchema from './PlanetSchema'

/**
 * The schema and ORM implenetation for the `Person` entity.
 * 
 * @author Stan Hurks
 */
@ObjectType()
@Entity({
    name: 'person'
})
export default class PersonSchema implements Person {

    /**
     * The UUID of the entity.
     */
    @PrimaryGeneratedColumn('uuid')
    @Field()
    uuid: string

    /**
     * The name of this person.
     */
    @Column()
    @Field()
    name: string

    /**
     * The birth year of the person, using the in -universe standard of BBY or ABY - Before the Battle of Yavin or After the Battle of Yavin.The Battle of Yavin is a battle that occurs at the end of Star Wars episode IV: A New Hope.
     */
    @Column()
    @Field()
    birth_year: string

    /**
     * The eye color of this person.Will be "unknown" if not known or "n/a" if the person does not have an eye.
     */
    @Column()
    @Field()
    eye_color: string

    /**
     * The gender of this person.Either "Male", "Female" or "unknown", "n/a" if the person does not have a gender.
     */
    @Column()
    @Field()
    gender: string

    /**
     * The hair color of this person.Will be "unknown" if not known or "n/a" if the person does not have hair.
     */
    @Column()
    @Field()
    hair_color: string

    /**
     * The height of the person in centimeters.
     */
    @Column()
    @Field()
    height: string

    /**
     * The mass of the person in kilograms.
     */
    @Column()
    @Field()
    mass: string

    /**
     * The skin color of this person.
     */
    @Column()
    @Field()
    skin_color: string

    /**
     * The URL of a planet resource, a planet that this person was born on or inhabits.
     */
    homeworld: Planet

    /**
     * An array of films that this person has been in.
     */
    @ManyToMany(type => FilmSchema)
    @JoinTable({
        name: 'person_film'
    })
    @Field(type => [FilmSchema])
    films: Film[]

    /**
     * An array of species that this person belongs to.
     */
    @ManyToMany(type => SpeciesSchema)
    @JoinTable({
        name: 'person_species'
    })
    @Field(type => [SpeciesSchema])
    species: Species[]

    /**
     * An array of starships that this person has piloted.
     */
    @ManyToMany(type => StarshipSchema)
    @JoinTable({
        name: 'person_starship'
    })
    @Field(type => [StarshipSchema])
    starships: Starship[]

    /**
     * An array of vehicles that this person has piloted.
     */
    @ManyToMany(type => VehicleSchema)
    @JoinTable({
        name: 'person_vehicle'
    })
    @Field(type => [VehicleSchema])
    vehicles: Vehicle[]

    /**
     * the time that this resource was created.
     */
    @Column({
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP'
    })
    @Field()
    created: Date

    /**
     * the time that this resource was edited.
     */
    @Column({
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP'
    })
    @Field()
    edited: Date
}