import Planet from 'greenberry-common/models/entity/Planet'
import Film from 'greenberry-common/models/entity/Film'
import { ObjectType, Field } from 'type-graphql'
import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable } from 'typeorm'
import FilmSchema from './FilmSchema'

/**
 * The schema and ORM implementation for the `Planet` entity.
 * 
 * @author Stan Hurks
 */
@ObjectType()
@Entity({
    name: 'planet'
})
export default class PlanetSchema implements Planet {

    /**
     * The UUID of the entity.
     */
    @PrimaryGeneratedColumn('uuid')
    @Field()
    uuid: string

    /**
     * The name of this planet.
     */
    @Column()
    @Field()
    name: string

    /**
     * The diameter of this planet in kilometers.
     */
    @Column()
    @Field()
    diameter: string

    /**
     * The number of standard hours it takes for this planet to complete a single rotation on its axis.
     */
    @Column()
    @Field()
    rotation_period: string

    /**
     * The number of standard days it takes for this planet to complete a single orbit of its local star.
     */
    @Column()
    @Field()
    orbital_period: string

    /**
     * A number denoting the gravity of this planet, where "1" is normal or 1 standard G. "2" is twice or 2 standard Gs. "0.5" is half or 0.5 standard Gs.
     */
    @Column()
    @Field()
    gravity: string

    /**
     * The average population of sentient beings inhabiting this planet.
     */
    @Column()
    @Field()
    population: string

    /**
     * The climate of this planet.Comma separated if diverse.
     */
    @Column()
    @Field()
    climate: string

    /**
     * The terrain of this planet.Comma separated if diverse.
     */
    @Column()
    @Field()
    terrain: string

    /**
     * The percentage of the planet surface that is naturally occurring water or bodies of water.
     */
    @Column()
    @Field()
    surface_water: string

    /**
     * An array of Film URL Resources that this planet has appeared in.
     */
    @ManyToMany(type => FilmSchema)
    @JoinTable({
        name: 'planet_film'
    })
    @Field(type => [FilmSchema])
    films: Film[]

    /**
     * The time that this resource was created.
     */
    @Column({
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP'
    })
    @Field()
    created: Date

    /**
     * The time that this resource was edited.
     */
    @Column({
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP'
    })
    @Field()
    edited: Date
}