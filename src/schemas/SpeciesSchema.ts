import Species from 'greenberry-common/models/entity/Species'
import { Field, ObjectType } from 'type-graphql'
import { Column, PrimaryGeneratedColumn, JoinTable, ManyToMany, Entity } from 'typeorm'
import Film from 'greenberry-common/models/entity/Film'
import FilmSchema from './FilmSchema'
import Planet from 'greenberry-common/models/entity/Planet'
import PlanetSchema from './PlanetSchema'

/**
 * The schema and ORM implementation for the `Species` entity.
 * 
 * @author Stan Hurks
 */
@ObjectType()
@Entity({
    name: 'species'
})
export default class SpeciesSchema implements Species {

    /**
     * The UUID of the entity.
     */
    @PrimaryGeneratedColumn('uuid')
    @Field()
    uuid: string

    /**
     * The name of this species.
     */
    @Column()
    @Field()
    name: string

    /**
     * The classification of this species, such as "mammal" or "reptile".
     */
    @Column()
    @Field()
    classification: string

    /**
     * The designation of this species, such as "sentient".
     */
    @Column()
    @Field()
    designation: string

    /**
     * The average height of this species in centimeters.
     */
    @Column()
    @Field()
    average_height: string

    /**
     * The average lifespan of this species in years.
     */
    @Column()
    @Field()
    average_lifespan: string

    /**
     * A comma - separated string of common eye colors for this species, "none" if this species does not typically have eyes.
     */
    @Column()
    @Field()
    eye_colors: string

    /**
     * A comma - separated string of common hair colors for this species, "none" if this species does not typically have hair.
     */
    @Column()
    @Field()
    hair_colors: string

    /**
     * A comma - separated string of common skin colors for this species, "none" if this species does not typically have skin.
     */
    @Column()
    @Field()
    skin_colors: string

    /**
     * The language commonly spoken by this species.
     */
    @Column()
    @Field()
    language: string

    /**
     * The URL of a planet resource, a planet that this species originates from.
     */
    @ManyToMany(type => PlanetSchema)
    @JoinTable({
        name: 'species_planet'
    })
    @Field(type => PlanetSchema)
    homeworld: Planet

    /**
     * An array of Film URL Resources that this species has appeared in.
     */
    @ManyToMany(type => FilmSchema)
    @JoinTable({
        name: 'species_film'
    })
    @Field(type => [FilmSchema])
    films: Film[]

    /**
     * A date created from the ISO 8601 date format of the time that this resource was created.
     */
    @Column({
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP'
    })
    @Field()
    created: Date

    /**
     * A date converted from the ISO 8601 date format of the time that this resource was edited.
     */
    @Column({
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP'
    })
    @Field()
    edited: Date
}