import Starship from 'greenberry-common/models/entity/Starship'
import Person from 'greenberry-common/models/entity/Person'
import Film from 'greenberry-common/models/entity/Film'
import { Field, ObjectType } from 'type-graphql'
import { Column, PrimaryGeneratedColumn, ManyToMany, JoinTable, Entity } from 'typeorm'
import FilmSchema from './FilmSchema'
import PersonSchema from './PersonSchema'

/**
 * The schema and ORM implementation for the `Starship` entity.
 * 
 * @author Stan Hurks
 */
@ObjectType()
@Entity({
    name: 'starship'
})
export default class StarshipSchema implements Starship {

    /**
     * The UUID of the entity.
     */
    @PrimaryGeneratedColumn('uuid')
    @Field()
    uuid: string

    /**
     * The name of this starship.The common name, such as "Death Star".
     */
    @Column()
    @Field()
    name: string

    /**
     * The model or official name of this starship.Such as "T-65 X-wing" or "DS-1 Orbital Battle Station".
     */
    @Column()
    @Field()
    model: string

    /**
     * The class of this starship, such as "Starfighter" or "Deep Space Mobile Battlestation"
     */
    @Column()
    @Field()
    starship_class: string

    /**
     * The manufacturer of this starship.Comma separated if more than one.
     */
    @Column()
    @Field()
    manufacturer: string

    /**
     * The cost of this starship new, in galactic credits.
     */
    @Column()
    @Field()
    cost_in_credits: string

    /**
     * The length of this starship in meters.
     */
    @Column()
    @Field()
    length: string

    /**
     * The number of personnel needed to run or pilot this starship.
     */
    @Column()
    @Field()
    crew: string

    /**
     * The number of non - essential people this starship can transport.
     */
    @Column()
    @Field()
    passengers: string

    /**
     * The maximum speed of this starship in the atmosphere. "N/A" if this starship is incapable of atmospheric flight.
     */
    @Column()
    @Field()
    max_atmosphering_speed: string

    /**
     * The class of this starships hyperdrive.
     */
    @Column()
    @Field()
    hyperdrive_rating: string

    /**
     * The Maximum number of Megalights this starship can travel in a standard hour.A "Megalight" is a standard unit of distance and has never been defined before within the Star Wars universe.This figure is only really useful for measuring the difference in speed of starships.We can assume it is similar to AU, the distance between our Sun(Sol) and Earth.
     */
    @Column()
    @Field()
    MGLT: string

    /**
     * The maximum number of kilograms that this starship can transport.
     */
    @Column()
    @Field()
    cargo_capacity: string

    /**
     * The Maximum number of Megalights this starship can travel in a standard hour. A "Megalight" is a standard unit of distance and has never been defined before within the Star Wars universe. This figure is only really useful for measuring the difference in speed of starships. We can assume it is similar to AU, the distance between our Sun (Sol) and Earth.
     */
    @Column()
    @Field()
    consumables: string

    /**
     * An array of Film URL Resources that this starship has appeared in.
     */
    @ManyToMany(type => FilmSchema)
    @JoinTable({
        name: 'starship_film'
    })
    @Field(type => [FilmSchema])
    films: Film[]

    /**
     * The time that this resource was created.
     */
    @Column({
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP'
    })
    @Field()
    created: Date

    /**
     * The time that this resource was edited.
     */
    @Column({
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP'
    })
    @Field()
    edited: Date
}