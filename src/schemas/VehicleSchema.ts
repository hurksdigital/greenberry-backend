import Vehicle from 'greenberry-common/models/entity/Vehicle'
import { ObjectType, Field } from 'type-graphql'
import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable } from 'typeorm'
import Film from 'greenberry-common/models/entity/Film'
import FilmSchema from './FilmSchema'

/**
 * The schema and ORM implementation for the `Vehicle` entity.
 * 
 * @author Stan Hurks
 */
@ObjectType()
@Entity({
    name: 'vehicle'
})
export default class VehicleSchema implements Vehicle {

    /**
     * The UUID of the entity.
     */
    @PrimaryGeneratedColumn('uuid')
    @Field()
    uuid: string

    /**
     * The name of this vehicle.The common name, such as "Sand Crawler" or "Speeder bike".
     */
    @Column()
    @Field()
    name: string

    /**
     * The model or official name of this vehicle.Such as "All-Terrain Attack Transport".
     */
    @Column()
    @Field()
    model: string

    /**
     * The class of this vehicle, such as "Wheeled" or "Repulsorcraft".
     */
    @Column()
    @Field()
    vehicle_class: string

    /**
     * The manufacturer of this vehicle.Comma separated if more than one.
     */
    @Column()
    @Field()
    manufacturer: string

    /**
     * The length of this vehicle in meters.
     */
    @Column()
    @Field()
    length: string

    /**
     * The cost of this vehicle new, in Galactic Credits.
     */
    @Column()
    @Field()
    cost_in_credits: string

    /**
     * The number of personnel needed to run or pilot this vehicle.
     */
    @Column()
    @Field()
    crew: string

    /**
     * The number of non - essential people this vehicle can transport.
     */
    @Column()
    @Field()
    passengers: string

    /**
     * The maximum speed of this vehicle in the atmosphere.
     */
    @Column()
    @Field()
    max_atmosphering_speed: string

    /**
     * The maximum number of kilograms that this vehicle can transport.
     */
    @Column()
    @Field()
    cargo_capacity: string

    /**
     *The maximum length of time that this vehicle can provide consumables for its entire crew without having to resupply.
    */
    @Column()
    @Field()
    consumables: string

    /**
     * An array of Film URL Resources that this vehicle has appeared in.
     */
    @ManyToMany(type => FilmSchema)
    @JoinTable({
        name: 'vehicle_film'
    })
    @Field(type => [FilmSchema])
    films: Film[]

    /**
     * the ISO 8601 date format of the time that this resource was created.
     */
    @Column({
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP'
    })
    @Field()
    created: Date

    /**
     * the ISO 8601 date format of the time that this resource was edited.
     */
    @Column({
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP'
    })
    @Field()
    edited: Date
}